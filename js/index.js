var number1 = 0;    //Variable for Number in the big Input
var number2 = 0;    //Variable for Number in equation Input

var bigDisplay = document.getElementById("bigDisplay");
var equationDisplay = document.getElementById("equationDisplay");

var clearDisplayOnNextInput = false;

//Stores last pressed Operation (Add, Subtract, Divide, Multiply)
var lastOperation = ""; 

function number(input)
{
    //Check if Operation has happened
    if(clearDisplayOnNextInput)
    {
        bigDisplay.value = "";
        clearDisplayOnNextInput = false;
    }

    switch(input)
    {
        case 0:
            bigDisplay.value += String(0);
            break;
        case 1:
            bigDisplay.value += String(1);
            break;
        case 2:
            bigDisplay.value += String(2);
            break;
        case 3:
            bigDisplay.value += String(3);
            break;
        case 4:
            bigDisplay.value += String(4);
            break;
        case 5:
            bigDisplay.value += String(5);
            break;
        case 6:
            bigDisplay.value += String(6);
            break;
        case 7:
            bigDisplay.value += String(7);
            break;
        case 8:
            bigDisplay.value += String(8);
            break;
        case 9:
            bigDisplay.value += String(9);
            break;
        case '.':
            if(bigDisplay.value.includes("."))
                break;
            
            bigDisplay.value += ".";
            break;
    }
}

function calculate()
{
    number2 = Number(equationDisplay.value.substring(0, equationDisplay.value.length-2));
    number1 = Number(bigDisplay.value);
    equationDisplay.value = "";

    switch(lastOperation)
    {
        case "Add":
            bigDisplay.value = String(number2+number1);
            break;
        case "Subtract":
            bigDisplay.value = String(number2-number1);
            break;
        case "Multiply":
            bigDisplay.value = String(number2*number1);
            break;
        case "Divide":
            bigDisplay.value = String(number2/number1);
            break;
    }
    lastOperation = "";
}

function add()
{
    if(bigDisplay.value == "")
        return;

    if(equationDisplay.value != "") //In both Inputs there is no blank value
        calculate();

    equationDisplay.value = bigDisplay.value + " +";
    lastOperation = "Add";
    clearDisplayOnNextInput = true;
}

function subtract()
{
    if(bigDisplay.value == "" || clearDisplayOnNextInput) //Use subtract button for sign
    {
        bigDisplay.value = "";
        bigDisplay.value += "-";
        clearDisplayOnNextInput = false;
    }   
    else  //Use Subtract Button for Subtraction
    {
        if(equationDisplay.value != "") //In both Inputs there is no blank value
        calculate();

        equationDisplay.value = bigDisplay.value + " -";
        lastOperation = "Subtract";
        clearDisplayOnNextInput = true;
    }   
}

function divide()
{
    if(bigDisplay.value == "")
        return;

    if(equationDisplay.value != "") //In both Inputs there is no blank value
        calculate();

    equationDisplay.value = bigDisplay.value + " ÷";
    lastOperation = "Divide";
    clearDisplayOnNextInput = true;
}

function multiply()
{
    if(bigDisplay.value == "")
        return;

    if(equationDisplay.value != "") //In both Inputs there is no blank value
        calculate();

    equationDisplay.value = bigDisplay.value + " ×";
    lastOperation = "Multiply";
    clearDisplayOnNextInput = true;
}